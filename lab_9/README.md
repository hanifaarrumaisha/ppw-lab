## Checklist

### Mandatory 
1. Session: Login & Logout
    1. [X] Implementasi fungsi Login 
        * Pertama membuat fungsi index yang mengecek apakah user telah login atau belum dengan mengecek user_login di session, apabila ada maka akan dialihkan ke url dengan nama profile, kalau tidak ada maka akan diarahkan ke html login yang ada di session.
        * membuat **HTML** login yang apabila di klik akan menuju ke url custom auth
        * membuat **URL** untuk menuju ke custom auth
        * membuat **Auth** dalam hal ini custom auth yang telah disediakan dimana didalamnya mengeset session, dibuat fungsi login auth yang memanggil api csui: 
            ```
                from django.shortcuts import render
                from django.contrib import messages
                from django.http import HttpResponseRedirect
                from django.urls import reverse
                from .csui_helper import get_access_token, verify_user
        
                #authentication
                def auth_login(request):
                    print ("#==> auth_login ")
        
                    if request.method == "POST":
                        username = request.POST['username']
                        password = request.POST['password']
        
                    #call csui_helper
                    try:
                        access_token = get_access_token(username, password)
                        ver_user = verify_user(access_token)
                        kode_identitas = ver_user['identity_number']
                        role = ver_user['role'].capitalize()
        
                        # set session
                        request.session['user_login'] = username
                        request.session['access_token'] = access_token
                        request.session['kode_identitas'] = kode_identitas
                        request.session['role'] = role
                        messages.success(request, "Anda berhasil login")
                    except Exception as e:
                        messages.error(request, "Username atau password salah")
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            ```
        * Membuat **HTML** di base.html untuk message yang menampilkan message yang dapat ditampilkan:
            ```
                {% for message in messages %}
                <div class="alert {{ message.tags }} alert-dismissible" role="alert" id="django-messages">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-right: 15px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ message }}
                </div>
                {% endfor %}
            ```
    2. [X] Implementasi fungsi Logout 
        * Membuat button di **HTML** untuk logout yang memanggil url auth_logout:
        * Membuat **URL** dengan nama auth_logout yang menuju ke file custom_logout function auth_logout
        * Membuat function auth_logout di dalam custom_logout:
        ```
            def auth_logout(request):
                print ("#==> auth logout")
                request.session.flush() # menghapus semua session
            
                messages.info(request, "Anda berhasil logout. Semua session Anda sudah dihapus")
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        ```
        
2. Session: Kelola Favorit
    1. [X] Implementasi fungsi "Favoritkan" untuk Drones
        * Membuat function add_session_drones di **views.py**:
            ```
                def add_session_drones(request, id):
                    ssn_key = request.session.keys()
                    if not 'drones' in ssn_key:
                        print ("# init drones ")
                        request.session['drones'] = [id]
                    else:
                        drones = request.session['drones']
                        print ("# existing drones => ", drones)
                        if id not in drones:
                            print ('# add new item, then save to session')
                            drones.append(id)
                            request.session['drones'] = drones
                    messages.success(request, "Berhasil tambah drone favorite")
                    return HttpResponseRedirect(reverse('lab-9:profile'))
            ```
            *session keys itu apa ya?*
        * Membuat tambahan di **urls.py**:
            ```
                url(r'^add_session_drones/(?P<id>\d+)/$', add_session_drones, name='add_session_drones'),
            ```
        * Membuat button di **table/drones.html** jika drone.id **tidak berada** di fav_drones yang merupakan response berisi session['drones']:
            ```
                % if not drone.id in fav_drones %}
                <a href="{% url 'lab-9:add_session_drones' drone.id %}" class="btn btn-primary"> Favoritkan </a>
            ```
    2. [X] Implementasi fungsi "Hapus dari favorit" untuk Drones
        * membuat function del_session_drone di **views.py**:
            ```
                def del_session_drones(request, id):
                    print ("# DEL drones")
                    drones = request.session['drones']
                    print ("before = ", drones)
                    drones.remove(id) #untuk remove id tertentu dari list
                    request.session['drones'] = drones
                    print ("after = ", drones)
                
                    messages.error(request, "Berhasil hapus dari favorite")
                    return HttpResponseRedirect(reverse('lab-9:profile'))
            ```
        * Membuat tambahan di **urls.py**:
            ```
                url(r'^del_session_drones/(?P<id>\d+)/$', del_session_drones, name='del_session_drones'),
            ```
        * Membuat button di **table/drones.html** bila drone.id **berada** di fav_drones:
            ```
                {% else %}
                <a href="{%url 'lab-9:del_session_drones' drone.id %}" class="btn btn-primary"> Hapus dari favorit </a>
                {% endif %}
            ```
    3. [X] Implementasi fungsi "Reset favorit" untuk Drones
        * Membuat function clear_session_drones di **views.py**:
            ```
                def clear_session_drones(request):
                    print ("# CLEAR session drones")
                    print ("before 1 = ", request.session['drones'])
                    del request.session['drones']
                
                    messages.error(request, "Berhasil reset favorite drones")
                    return HttpResponseRedirect(reverse('lab-9:profile'))
            ```
        * Membuat tambahan di **urls.py**:
            ```
                url(r'^clear_session_drones/$', clear_session_drones, name='clear_session_drones'),
            ```
        * Membuat button reset di **tables/drones.html**:
            ```
                <a href="{% url 'lab-9:clear_session_drones' %}" class="btn btn-danger" onclick="return confirm('Reset data?')">
                    Reset Favorite Drones
                </a>
            ```
        
3. Cookies: Login & Logout
    1. [X] Implementasi fungsi Login
        * Membuat function cookie_auth_login dan cookie_login di **views.py**:
            ```    
                def cookie_login(request):
                    print ("#==> masuk login")
                    if is_login(request):
                        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
                    else:
                        html = 'lab_9/cookie/login.html'
                        return render(request, html, response)
                
                def cookie_auth_login(request):
                    print ("# Auth login")
                    if request.method == "POST":
                        user_login = request.POST['username']
                        user_password = request.POST['password']
                
                        if my_cookie_auth(user_login, user_password):
                            print ("#SET cookies")
                            res = HttpResponseRedirect(reverse('lab-9:cookie_login'))
                
                            res.set_cookie('user_login', user_login)
                            res.set_cookie('user_password', user_password)
                
                            return res
                        else:
                            msg = "Username atau Password Salah"
                            messages.error(request, msg)
                            return HttpResponseRedirect(reverse('lab-9:cookie_login'))
                    else:
                        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
                    
                def cookie_profile(request):
                    print ("# cookie profile ")
                    # method ini untuk mencegah error ketika akses URL secara langsung
                    if not is_login(request):
                        print ("belum login")
                        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
                    else:
                        # print ("cookies => ", request.COOKIES)
                        in_uname = request.COOKIES['user_login']
                        in_pwd = request.COOKIES['user_password']
                
                        # jika cookie diset secara manual (usaha hacking), distop dengan cara berikut
                        # agar bisa masuk kembali, maka hapus secara manual cookies yang sudah diset
                        if my_cookie_auth(in_uname, in_pwd):
                            html = "lab_9/cookie/profile.html"
                            res =  render(request, html, response)
                            return res
                        else:
                            print ("#login dulu")
                            msg = "Kamu tidak punya akses :P "
                            messages.error(request, msg)
                            html = "lab_9/cookie/login.html"
                            return render(request, html, response)
            ```
        * Membuat baris kode ini di **urls.py**:
            ```
                url(r'^cookie/login/$', cookie_login, name='cookie_login'),
                url(r'^cookie/auth_login/$', cookie_auth_login, name='cookie_auth_login'),
                url(r'^cookie/profile/$', cookie_profile, name='cookie_profile'),
            ```
        * Membuat file **cookie/login.html**:
            ```
                {% extends "lab_9/layout/base.html" %}
                {% block content %}
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="rata-tengah">
                            <div class="judul">
                                <h1> Login menggunakan COOKIES </h1>
                                <p class="text-danger"> Jangan menggunakan <b> akun SSO asli </b> </p>
                                <p class="text-danger"> karena Username dan password akan disimpan di dalam cookie </p>
                            </div>
                            <form action="{% url 'lab-9:cookie_auth_login' %}" method="POST">
                                {% csrf_token %}
                                <p>
                                    <label for="username"> Your username* </label>
                                    <input type="text" id="username" name="username" required>
                                </p>
                                <p>
                                    <label for="password"> Your password* </label>
                                    <input type="password" id="password" name="password" required>
                                </p>
                                <input type="submit" class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
                {% endblock %}
            ```
            
    2. [X] Implementasi fungsi Logout
        * Membuat function berikut di **views.py**:
            ```
                def cookie_clear(request):
                    res = HttpResponseRedirect(reverse('lab-9:cookie_login'))
                    res.delete_cookie('lang')
                    res.delete_cookie('user_login')
                    res.delete_cookie('user_password')
                
                    msg = "Anda berhasil logout. Cookies direset"
                    messages.info(request, msg)
                    return res
            ```
        * Membuat baris kode ini di **urls.py**:
            ```
                url(r'^cookie/clear/$', cookie_clear, name='cookie_clear'), #sekaligus logout dari cookie
            ```
        * Membuat file **cookies/profile.html***:
            ```
                {% extends "lab_9/layout/base.html" %}
                {% block content %}
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2> [Cookie] Profile </h2>
                    </div>
                    <div class="panel-body">
                        <p> Username : {{ request.COOKIES.user_login }} </p>
                    </div>
                    <div class="panel-footer">
                        <a href="{% url 'lab-9:cookie_clear' %}" class="btn btn-danger"> Reset Cookies (Logout) </a>
                    </div>
                </div>
                {% endblock %}
            ```

4. Implementasi Header dan Footer
    1. [X] Buatlah header yang berisi tombol Logout *hanya jika* sudah login 
    (baik pada session dan cookies). Buatlah sebagus dan semenarik mungkin.
        * Memberikan response di function index **views.py** bernama login yang bernilai false jika 'user_login' tidak ada di session:
            ```
                def index(request):
                    print ("#==> masuk index")
                    if 'user_login' in request.session:
                        return HttpResponseRedirect(reverse('lab-9:profile'))
                    else:
                        response['login'] = False
                        html = 'lab_9/session/login.html'
                        return render(request, html, response)
            ```
        * Memberikan response di set_data_for_session pada **views.py** login yang bernilai true, karena set_data_for_session diakses oleh function profile yang hanya akan dipanggil bila sudah login
            ```
                def set_data_for_session(res, request):
                    response['author'] = request.session['user_login']
                    response['access_token'] = request.session['access_token']
                    response['kode_identitas'] = request.session['kode_identitas']
                    response['role'] = request.session['role']
                    response['drones'] = get_drones().json()
                
                    # print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
                    ## handling agar tidak error saat pertama kali login (session kosong)
                    if 'drones' in request.session.keys():
                        response['fav_drones'] = request.session['drones']
                    # jika tidak ditambahkan else, cache akan tetap menyimpan data
                    # sebelumnya yang ada pada response, sehingga data tidak up-to-date
                    else:
                        response['fav_drones'] = []
            ```
        * membuat code ini di **header.html**:
            ```
                {% if login %}
                    <div id="navbar" class="navbar-collapse collapse">
                      <ul class="nav navbar-nav nav-right pull-right">
                        <li><a href="{% url 'lab-9:auth_logout' %}" onclick="return confirm('Keluar?')">Logout</a></li>
                      </ul>
                    </div>
                    {% endif %}
            ```
3. Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [X] Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [X] Pastikan _Code Coverage_ kalian 100%

### Challenge
1. Implementasi API Optical dan SoundCard
    1. [X] Menambahkan link ke tab Optical dan Soundcard pada halaman Session Profile
        * Menambahkan fungsi berikut pada **api_enterkomputer.py**:
            ```
                # lengkapi pemanggilan utk SOUNDCARD_API dan OPTICAL_API untuk mengerjakan CHALLENGE
                def get_soundcards():
                    soundcards = requests.get(SOUNDCARD_API)
                    return soundcards
                
                def get_opticals():
                    opticals = requests.get(OPTICAL_API)
                    return opticals
            ```
        * Menambahkan baris kode berikut ke **session/profile.html**:
            ```
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#drones" aria-controls="home" role="tab" data-toggle="tab"> Drones </a>
                    </li>
                    <li role="presentation">
                        <a href="#soundcards" aria-controls="settings" role="tab" data-toggle="tab"> Soundcard </a>
                    </li>
                    <li role="presentation">
                        <a href="#opticals" aria-controls="settings" role="tab" data-toggle="tab"> Optical </a>
                    </li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="drones">
                        {% include 'lab_9/tables/drones.html' %}
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="soundcards">
                        <!-- Apply the same here -->
                        {% include 'lab_9/tables/soundcards.html' %}
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="opticals">
                        <!-- Apply the same here -->
                        {% include 'lab_9/tables/opticals.html' %}
                    </div>
                </div>
            ```
    2. [X] Membuat tabel berisi data optical/soundcard 
        * Memngubah function berikut di **views.py**:
            ```
                def set_data_for_session(res, request):
                    response['author'] = request.session['user_login']
                    response['access_token'] = request.session['access_token']
                    response['kode_identitas'] = request.session['kode_identitas']
                    response['role'] = request.session['role']
                    response['login'] = True
                    response['drones'] = get_drones().json()
                    response['soundcards'] = get_soundcards().json()
                    response['opticals'] = get_opticals().json()
                
                    # print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
                    ## handling agar tidak error saat pertama kali login (session kosong)
                    if 'drones' in request.session.keys():
                        response['fav_drones'] = request.session['drones']
                    # jika tidak ditambahkan else, cache akan tetap menyimpan data
                    # sebelumnya yang ada pada response, sehingga data tidak up-to-date
                    
                    else:
                        response['fav_drones'] = []
                    if 'soundcards' in request.session.keys():
                        response['fav_soundcards'] = request.session['soundcards']
                    else:
                        response['fav_soundcards'] = []
                    if 'opticals' in request.session.keys():
                        response['fav_opticals'] = request.session['opticals']
                    else:
                        response['fav_opticals'] = []
            ```
        * Membuat file **tables/opticals.html**:
            ```
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h2> Daftar Optical : {{ opticals | length}} </h2>
                        <a href="{% url 'lab-9:clear_session_item' 'opticals' %}" class="btn btn-danger" onclick="return confirm('Reset data?')">
                            Reset Favorite Optical
                        </a>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <th> No</th>
                            <th> Nama</th>
                            <th> Harga</th>
                            <th> Jumlah</th>
                            <th> Aksi </th>
                
                            </thead>
                            <tbody>
                            {% for optical in opticals %}
                            <tr>
                                <td> {{ forloop.counter }}</td>
                                <td> {{ optical.name }}</td>
                                <td> {{ optical.price }}</td>
                                <td> {{ optical.quantity }}</td>
                                <td>
                                    {% if not optical.id in fav_opticals %}
                                    <a href="{% url 'lab-9:add_session_item' 'opticals' optical.id %}" class="btn btn-primary"> Favoritkan </a>
                                    {% else %}
                                    <a href="{%url 'lab-9:del_session_item' 'opticals' optical.id %}" class="btn btn-primary"> Hapus dari favorit </a>
                                    {% endif %}
                                </td>
                            </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            ```
        * Membuat file **tables/soundcards.html**
            ```
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h2> Daftar Sound Card : {{ soundcards | length}} </h2>
                        <a href="{% url 'lab-9:clear_session_item' 'soundcards' %}" class="btn btn-danger" onclick="return confirm('Reset data?')">
                            Reset Favorite Soundcard
                        </a>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <th> No</th>
                            <th> Nama</th>
                            <th> Harga</th>
                            <th> Jumlah</th>
                            <th> Aksi </th>
                
                            </thead>
                            <tbody>
                            {% for soundcard in soundcards %}
                            <tr>
                                <td> {{ forloop.counter }}</td>
                                <td> {{ soundcard.name }}</td>
                                <td> {{ soundcard.price }}</td>
                                <td> {{ soundcard.quantity }}</td>
                                <td>
                                    {% if not soundcard.id in fav_soundcards %}
                                    <a href="{% url 'lab-9:add_session_item' 'soundcards' soundcard.id %}" class="btn btn-primary"> Favoritkan </a>
                                    {% else %}
                                    <a href="{%url 'lab-9:del_session_item' 'soundcards' soundcard.id %}" class="btn btn-primary"> Hapus dari favorit </a>
                                    {% endif %}
                                </td>
                            </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            ```
            
2. Implementasi fungsi umum yang sudah disediakan mengelola session:
    1. [X] Menggunakan fungsi umum untuk menambahkan (Favoritkan) optical/soundcard ke session
        * Membuat fungsi berikut di **views.py**
            ```
                def add_session_item(request, key, id):
                    print ("#ADD session item")
                    ssn_key = request.session.keys()
                    if not key in ssn_key:
                        request.session[key] = [id]
                    else:
                        items = request.session[key]
                        if id not in items:
                            items.append(id)
                            request.session[key] = items
                
                    msg = "Berhasil tambah " + key[:-1] +" favorite"
                    messages.success(request, msg)
                    return HttpResponseRedirect(reverse('lab-9:profile'))
            ```
        * Menambah baris kode berikut di **urls.py**:
            ```
                #general function : solution to challenge
                url(r'^add_session_item/(?P<key>\w+)/(?P<id>\d+)/$', add_session_item, name='add_session_item'),
            ```
    2. [X] Menggunakan fungsi umum untuk menghapus (Hapus dari Favorit) optical/soundcard dari session
        * Membuat fungsi berikut id **views.py**:
            ```
                def del_session_item(request, key, id):
                    print ("# DEL session item")
                    items = request.session[key]
                    print ("before = ", items)
                    items.remove(id)
                    request.session[key] = items
                    print ("after = ", items)
                
                    msg = "Berhasil hapus item " + key[:-1] + " dari favorite"
                    messages.error(request, msg)
                    return HttpResponseRedirect(reverse('lab-9:profile'))
            ```
        * Menambah baris kode berikut di **urls.py**:
            ```
                url(r'^del_session_item/(?P<key>\w+)/(?P<id>\d+)/$', del_session_item, name='del_session_item'),
            ```
    3. [X] Menggunakan fungsi umum untuk menghapus/reset kategori (drones/optical/soundcard) dari session.
        * Membuat fungsi berikut di **views.py**
            ```
                def del_session_item(request, key, id):
                    print ("# DEL session item")
                    items = request.session[key]
                    print ("before = ", items)
                    items.remove(id)
                    request.session[key] = items
                    print ("after = ", items)
                
                    msg = "Berhasil hapus item " + key[:-1] + " dari favorite"
                    messages.error(request, msg)
                    return HttpResponseRedirect(reverse('lab-9:profile'))
            ```
        * Menambah baris kode berikut di **urls.py**:
            ```
                url(r'^clear_session_item/(?P<key>\w+)/$', clear_session_item, name='clear_session_item'),
            ```
        
3. Implementasi _session_ untuk semua halaman yang telah dibuat pada Lab Sebelumnya
    1. [ ] Jika halaman lab diakses tanpa login terlebih dahulu, maka mereka akan ditampilkan halaman login
    2. [ ] Ketika halaman Lab ke-**N** diakses tanpa login, maka setelah login, pengguna akan diberikan tampilan Lab ke-**N**
    3. [ ] Ubahlah implementasi `csui_helper.py` pada Lab 9 sehingga bisa digunakan oleh Lab 7 (Kalian boleh menghapus berkas `csui_helper.py`
    yang ada di Lab 7)
 
##Additional Notes
1. reverse itu untuk menunjuk suatu url tapi pakai url_name, biar nanti walaupun function atau url gonta-ganti jadinya ga perlu ngubah function di viewsnya